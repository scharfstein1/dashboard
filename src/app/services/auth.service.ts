import { Injectable } from '@angular/core';
import { UsuarioResponseI } from '../models/usuarioResponse';
import { UsuarioI } from '../models/usuario';
import { HttpClient, HttpClientJsonpModule, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, tap, map} from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment'

const jwt = new JwtHelperService();

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    URL_API:string = '/autenticar';
    loggedIn = new BehaviorSubject<boolean>(false);
    
    private token:string;

    constructor(private http:HttpClient, private router: Router) { 
        this.token = '';
    }

    get estaLogeado():Observable<boolean>{
        this.checkToken();
        return this.loggedIn.asObservable();
    }

    login(usuario: UsuarioI): Observable<UsuarioResponseI | void>{

        return this.http.post<UsuarioResponseI>(
            environment.api + this.URL_API, usuario
        )
        .pipe(
            map((res:UsuarioResponseI) => {
                if (res){                    
                    this.setToken(res.token);
                    this.loggedIn.next(true);
                    this.router.navigate(['dashboard']);
                }
            }),
            catchError( (error) => this.handleError(error))
        );

    }

    logout(): void {
        this.token = '';
        localStorage.removeItem('ACCESS_TOKEN');
        this.loggedIn.next(false);
        this.router.navigate(['login']);
    }

    private setToken(token:string): void {
        localStorage.setItem('ACCESS_TOKEN', token);
        
        this.token = token;
    }

    private checkToken(): void {
        const actualToken = localStorage.getItem('ACCESS_TOKEN') || undefined;

        const expirado = jwt.isTokenExpired(actualToken);
        
        if(expirado) {
            this.logout();
        }else{
            this.loggedIn.next(true);
        }
    }
    
    private handleError(error:any): Observable<never> {
        let mensaje = 'Error al obtener datos';

        if(error){
            mensaje = `Error: ${error.message}`;
        }

        console.error(mensaje);

        return throwError(error);
    }
}
