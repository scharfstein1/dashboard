import { Injectable } from '@angular/core';
import { HttpClient, HttpClientJsonpModule, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';
import { ValvulaI } from '../models/valvula';
import { SensorI } from '../models/sensor';
import { environment } from '../../environments/environment'

@Injectable({
    providedIn: 'root'
})
export class ChartService {

    constructor(private http: HttpClient) { }

    getSensor(tipo:string): Observable<Object | void>{
        return this.http.get(
            environment.api + '/registro/sensor',
            {
                params: {
                    tipo: tipo
                }
            }
        )
        .pipe(            
            catchError((error) => this.handleError(error))
        );
    }

    getValvula(tipo: string): Observable<Object | void> {
        return this.http.get(
            environment.api + '/registro/valvula',
            {
                params: {
                    tipo: tipo
                }
            }
        )
        .pipe(
            catchError((error) => this.handleError(error))
        );
    }

    toDateString(fechaTiempo:string, soloHoras:boolean = false){
        let d = new Date(fechaTiempo);

        if (soloHoras){
            return `${d.getHours()}:00`;
        }
        
        let fecha = `${d.getDate()}/${d.getMonth() + 1}/${d.getFullYear()}`;
        let tiempo = `${d.getHours()}:${d.getMinutes()}:${ d.getSeconds()}`;

        return `${fecha} ${tiempo}`;
    }

    private handleError(error: any): Observable<never> {
        let mensaje = 'Error al obtener datos';

        if (error) {
            mensaje = `Error: ${error.message}`;
        }

        console.error(mensaje);

        return throwError(mensaje);
    }

    procesarDataSensor(temperaturas: SensorI[]) {
        return {
            data: Object(temperaturas).map((item: SensorI) => {
                return item.valor;
            }),
            labels: Object(temperaturas).map((item: SensorI) => {
                return this.toDateString(item.creado);
            })
        }

    }


    procesarDataValvula(data: any) {

        const temps = data.reduce((temps: any, temp: ValvulaI) => {
            const date = this.toDateString(temp.creado, true);

            if (!temps[date]) {
                temps[date] = [];
            }

            temps[date].push(temp);
            return temps;

        }, {});

        let labels = Object.keys(temps).sort(function (a, b) {
            let anterior: number = Number.parseInt(a.split(':')[0]);
            let nueva: number = Number.parseInt(b.split(':')[0]);
            return anterior - nueva;
        });

        let chartData:any = [];
        labels.forEach(label => {
            chartData.push(temps[label].length);
        });

        return {
            data: chartData,
            labels: labels
        }
    }


}
