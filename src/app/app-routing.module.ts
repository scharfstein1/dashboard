import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CheckLoginGuard } from './guards/check-login.guard';
import { DashboardComponent } from './modules/dashboard/dashboard.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full'
    },
    {
        path: 'login',
        loadChildren: () => import('./modules/auth/auth.module').then(
            m => m.AuthModule
        ),
    },
    {
        path: 'dashboard',
        canActivate: [CheckLoginGuard],
        loadChildren: () => import('./modules/dashboard/dashboard.module').then(
            m => m.DashboardModule
        ),
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
