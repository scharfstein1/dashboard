import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { UsuarioI } from '../../../models/usuario';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    loginForm = new FormGroup({
        username: new FormControl('', Validators.required),
        password: new FormControl('', Validators.required)
    })

    error:string = '';
    
    constructor(private authService: AuthService, private router: Router) { }

    ngOnInit(): void { }

    onLogin(usuario: UsuarioI) {
        this.authService
            .login(usuario)
            .subscribe((data:any) => {                
                this.router.navigate(['dashboard']);
            },
            error => {
                switch(error.status){
                    case 401: 
                        this.error = "Credenciales invalidas";
                        break;
                    case 404: 
                        this.error = "Usuario invalido";
                        break;
                    default:
                        this.error = "Error al iniciar sesion";
                }
            });
    }



}
