import { Component, OnInit } from '@angular/core';
import { ValvulaI } from 'src/app/models/valvula';
import { ChartService } from 'src/app/services/chart.service';
import { ChartColor, ChartLabel, ChartsModule } from '@rinminase/ng-charts';

@Component({
  selector: 'hora-valvula',
  templateUrl: './hora-valvula.component.html',
  styleUrls: ['./hora-valvula.component.scss']
})
export class HoraValvulaComponent implements OnInit {


    constructor(private chartService: ChartService) { }
    ngOnInit(): void {
        this.getData();
    }
    tipo: string = 'temperatura';

    chartData = [
        { data: [], label: 'Apertura de valvula de agua' },
    ];
    chartLabels: ChartLabel[] = [];
    chartOptions = {
        responsive: true,
        scales: {
            yAxes: [{
                ticks: {
                    min: 0,
                    beginAtZero: true
                }
            }]
        }
    };

    getData() {

        this.chartService
            .getValvula(this.tipo)
            .subscribe((data: any) => {
                this.procesar(data);
            });

    }

    
    procesar(data: ValvulaI[]) {

        let chartData:any = this.chartService.procesarDataValvula(data);

        this.chartData[0].data = chartData.data;
        this.chartLabels = chartData.labels;
    }

}
