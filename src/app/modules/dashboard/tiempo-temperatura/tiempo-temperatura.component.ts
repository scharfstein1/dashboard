import { Component, OnInit, ViewChild } from '@angular/core';
import { SensorI } from 'src/app/models/sensor';
import { ChartService } from 'src/app/services/chart.service';
import { ChartColor, ChartLabel, ChartsModule} from '@rinminase/ng-charts';


@Component({
    selector: 'tiempo-temperatura',
    templateUrl: './tiempo-temperatura.component.html',
    styleUrls: ['./tiempo-temperatura.component.scss']
})

export class TiempoTemperaturaComponent implements OnInit {


    constructor(private chartService: ChartService) { }
    ngOnInit(): void {
        this.getData();
    }
    tipo:string = 'temperatura';

    chartData = [
        { data: [], label: 'Temperaturas' },
    ];
    chartLabels: ChartLabel[] = [];
    chartOptions = {
        responsive: true
    };

    getData() {

        this.chartService
            .getSensor(this.tipo)
            .subscribe((data: any) => {
                this.procesar(data);
            });

    }

    procesar(data: SensorI[]) {

        let chartData: any = this.chartService.procesarDataSensor(data);

        this.chartData[0].data = chartData.data;
        this.chartLabels = chartData.labels;

    }

}
