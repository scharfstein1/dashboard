import { Component, OnInit } from '@angular/core';
import { SensorI } from 'src/app/models/sensor';
import { ChartService } from 'src/app/services/chart.service';
import { ChartColor, ChartLabel, ChartsModule } from '@rinminase/ng-charts';

@Component({
  selector: 'tiempo-oxigeno',
  templateUrl: './tiempo-oxigeno.component.html',
  styleUrls: ['./tiempo-oxigeno.component.scss']
})
export class TiempoOxigenoComponent implements OnInit {

    constructor(private chartService: ChartService) { }
    ngOnInit(): void {
        this.getData();
    }

    tipo:string = 'oxigeno';

    chartData = [
        { data: [], label: 'Oxigeno', minBarLength: 0 },
    ];
    chartLabels: ChartLabel[] = [];
    chartOptions = {
        responsive: true
    };

    colors = [{
        backgroundColor: "rgba(126, 208, 255)",
        borderColor: "rgba(72, 188, 255)",
    }];

    getData() {

        this.chartService
            .getSensor(this.tipo)
            .subscribe((temperaturas: any) => {
                this.procesar(temperaturas);
            });

    }

    procesar(data: SensorI[]) {

        let chartData: any = this.chartService.procesarDataSensor(data);

        this.chartData[0].data = chartData.data;
        this.chartLabels = chartData.labels;

    }

}
