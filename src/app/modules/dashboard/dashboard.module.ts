import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { TiempoTemperaturaComponent } from './tiempo-temperatura/tiempo-temperatura.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { ChartsModule } from '@rinminase/ng-charts';
import { TiempoOxigenoComponent } from './tiempo-oxigeno/tiempo-oxigeno.component';
import { HoraValvulaComponent } from './hora-valvula/hora-valvula.component';
import { HoraOxigenoComponent } from './hora-oxigeno/hora-oxigeno.component';


@NgModule({
    declarations: [
        DashboardComponent,
        TiempoTemperaturaComponent,
        TiempoOxigenoComponent,
        HoraValvulaComponent,
        HoraOxigenoComponent
    ],
    imports: [
        CommonModule,
        DashboardRoutingModule,
        ChartsModule 
    ]
})

export class DashboardModule { }
 