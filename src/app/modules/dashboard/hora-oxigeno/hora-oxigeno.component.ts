import { Component, OnInit } from '@angular/core';
import { ValvulaI } from 'src/app/models/valvula';
import { ChartService } from 'src/app/services/chart.service';
import { ChartColor, ChartLabel, ChartsModule } from '@rinminase/ng-charts';

@Component({
  selector: 'hora-oxigeno',
  templateUrl: './hora-oxigeno.component.html',
  styleUrls: ['./hora-oxigeno.component.scss']
})
export class HoraOxigenoComponent implements OnInit {

    constructor(private chartService: ChartService) { }
    ngOnInit(): void {
        this.getData();
    }
    tipo: string = 'oxigeno';

    chartData = [
        { data: [], label: 'Apertura de valvula de oxigeno' },
    ];
    chartLabels: ChartLabel[] = [];
    chartOptions = {
        responsive: true,
        scales: {
            yAxes: [{
                ticks: {
                    min: 0,
                    beginAtZero: true
                }
            }]
        }
    };
    colors = [{
        backgroundColor: "rgba(126, 208, 255)",
        borderColor: "rgba(72, 188, 255)",
    }];

    getData() {

        this.chartService
            .getValvula(this.tipo)
            .subscribe((data: any) => {
                this.procesar(data);
            });

    }

    procesar(data: ValvulaI[]) {

        let chartData: any = this.chartService.procesarDataValvula(data);

        this.chartData[0].data = chartData.data;
        this.chartLabels = chartData.labels;
    }

}
